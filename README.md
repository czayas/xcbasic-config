# xcbasic-config

Estos archivos permiten la utilización del editor **Micro** para desarrollar aplicaciones usando **XC=BASIC**, un compilador cruzado multiplataforma que permite programar para la [Commodore 64](https://es.wikipedia.org/wiki/Commodore_64) desde un ambiente moderno como ser Linux, macOS o Windows.

## Archivos suministrados

- **init.lua**: Arranque de Micro Editor
- **xcbasic.yaml**: Resaltador de sintaxis para el código
- **xcbasic**: Script Bash para compilar y ejecutar
- **u64f**: Script Bash para copiar archivo a una Ultimate 64 vía FTP

## Requisitos

Se asume que los siguientes paquetes están instalados y accesibles a través del `PATH`:

- [Micro Editor](https://micro-editor.github.io/)
- [XC=BASIC](https://xc-basic.net/)
- [DASM Assembler](https://dasm-assembler.github.io/)
- [VICE](https://vice-emu.sourceforge.io/)

## Instrucciones de instalación

1. Copiar **init.lua** a `~/.config/micro`
2. Copiar **xcbasic.yaml** a `~/.config/micro/syntax`
3. Copiar **xcbasic** a alguna carpeta del `PATH`
4. Copiar **u64f** a alguna carpeta del `PATH`

## Uso

Abra o cree con **Micro Editor** un programa en **XC=BASIC** que tenga como extensión `.xcb` en su nombre de archivo. Contará con resaltado de sintaxis y la opción de compilar, ejecutar el código o transferirlo a una Ultimate 64 simplemente apretando la tecla **F5**. Adicionalmente, el archivo **init.lua** incorpora la misma funcionalidad al editar scripts **Bash** y **Python**.
