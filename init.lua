-- Para Micro Editor V2

local shell = import("micro/shell")
local config = import("micro/config")
local util = import("micro/util")

function preInsertNewline(bp)
    if bp.Buf:FileType() == "python" then
        local curLine = bp.Buf:Line(bp.Cursor.Y)
        if bp.Cursor.X == util.CharacterCountInString(curLine) and bp.Cursor.X > 0 and
                util.RuneAt(curLine, bp.Cursor.X-1) == ":" then
            bp:InsertNewline()
            bp:InsertTab()
            return false
        end
    end
    return true
end

function run(bp)
    local buf = bp.Buf
    local ftype = buf:FileType()
    local fname = '"'..buf.AbsPath..'"'
    if buf:Modified() then
        bp:Save()
    end
    if ftype == "shell" then
        shell.RunInteractiveShell('bash '..fname, true, false)
    elseif ftype == "python" or ftype == "python3" then
        shell.RunInteractiveShell('python3 '..fname, true, false)
    elseif ftype == "xcbasic" then
        shell.RunInteractiveShell('xcbasic '..fname..' output.prg', true, false)
    end
end

function init()
    config.TryBindKey("F5", "lua:initlua.run", true)
end
